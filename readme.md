# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: EKATERINA GERASIMOVA

**E-MAIL**: kogryster@gmail.com

# SOFTWARE

- JDK 1.8
- MS WINDOWS 7 / MS WINDOWS 10

# PROGRAM BUILD

```bash
mvn clean instal
```

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOT

**JSE-03**: https://yadi.sk/d/-Xg1JJV1FHbg7w?w=1

**JSE-04**: https://yadi.sk/d/fSp1aQDAFIGv-g?w=1

**JSE-05**: https://yadi.sk/d/a6c7eF1o1arD8w?w=1

**JSE-06**: https://yadi.sk/d/uuWH0kqk6Kxdjw?w=1